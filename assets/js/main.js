let items, itemsResult;
const cardsContainer = document.getElementById("cardsContainer");
const locationsContainer = document.getElementById("locationsContainer");
const episodesContainer = document.getElementById("episodesContainer");
const cards = document.getElementById("cards");
const locations = document.getElementById("locations");
const episodes = document.getElementById("episodes");
const search = document.getElementById("search");
const home = document.getElementById("home");

/* characters */
const btnCharacters = document.getElementById("btnCharacters");
btnCharacters.addEventListener("click", function() {
    loadDoc("https://rickandmortyapi.com/api/character", getCharacters);
});

const btnCharactersDropdown = document.getElementById("btnCharactersDropdown");
btnCharactersDropdown.addEventListener("click", function() {
    loadDoc("https://rickandmortyapi.com/api/character", getCharacters);
});

/* locations */
const btnLocations = document.getElementById("btnLocations");
btnLocations.addEventListener("click", function() {
    loadDoc("https://rickandmortyapi.com/api/location", getLocations);
});

const btnLocationsDropdown = document.getElementById("btnLocationsDropdown");
btnLocationsDropdown.addEventListener("click", function() {
    loadDoc("https://rickandmortyapi.com/api/location", getLocations);
});

/* episodes */
const btnEpisodes = document.getElementById("btnEpisodes");
btnEpisodes.addEventListener("click", function() {
    loadDoc("https://rickandmortyapi.com/api/episode", getEspisodes);
});

const btnEpisodesDropdown = document.getElementById("btnEpisodesDropdown");
btnEpisodesDropdown.addEventListener("click", function() {
    loadDoc("https://rickandmortyapi.com/api/episode", getEspisodes);
});

function loadDoc(url, cFunction) {
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            itemsResult = this.responseText;
            items = JSON.parse(itemsResult);

            cFunction(this);
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}

function getCharacters() {
    showCharacters();

    let rowCard = document.createElement("div");
    rowCard.className += "row row-cols-1 row-cols-md-2";

    for (let i = 0; i < items.results.length; i++) {

        let colCardContainer = document.createElement("div");
        colCardContainer.className += "col-card-container";

        let card = document.createElement("div");
        card.className += "card mb-3";

        let row = document.createElement("div");
        row.className += "row";

        /*card left - image */
        let colIntoCard4 = document.createElement("div");
        colIntoCard4.className += "col-md-4";

        let img = document.createElement("img");

        img.setAttribute("src", items.results[i].image);

        /*card right - info */
        let colIntoCard8 = document.createElement("div");
        colIntoCard8.className += "col-md-8";

        let cardBody = document.createElement("div");
        cardBody.className += "card-body";

        /* section - title */
        let section1 = document.createElement("div");
        section1.className += "section";

        let cardTitle1 = document.createElement("h4");
        cardTitle1.className += "card-title";
        cardTitle1.innerHTML = items.results[i].name;

        let state = document.createElement("p");
        state.className += "state";

        let statusIcon = document.createElement("div");
        statusIcon.className += "status-icon " + items.results[i].status;

        let stateInfo = document.createElement("p");
        stateInfo.className += "state-info";

        stateInfo.innerHTML = items.results[i].status + " - " + items.results[i].species;

        /* section - location */
        let section2 = document.createElement("div");
        section2.className += "section";

        let location = document.createElement("p");
        location.className += "location";
        location.innerHTML = "Last known location:";

        let locationInfo = document.createElement("p");
        locationInfo.innerHTML = items.results[i].location.name;

        /* section - last seen*/
        let section3 = document.createElement("div");
        section3.className += "section";

        let seen = document.createElement("p");
        seen.className += "seen";
        seen.innerHTML = "First seen in:";

        let seenInfo = document.createElement("p");
        seenInfo.innerHTML = items.results[i].origin.name;

        /*card left*/
        cardsContainer.appendChild(rowCard);
        rowCard.appendChild(colCardContainer);
        colCardContainer.appendChild(card);
        card.appendChild(row);
        row.appendChild(colIntoCard4);
        row.appendChild(colIntoCard8);
        colIntoCard4.appendChild(img);
        colIntoCard8.appendChild(cardBody);

        /* section - title */
        cardBody.appendChild(section1);
        section1.appendChild(cardTitle1);
        section1.appendChild(state);
        state.appendChild(statusIcon);
        state.appendChild(stateInfo);

        /* section - location */
        cardBody.appendChild(section2);
        section2.appendChild(location);
        section2.appendChild(locationInfo);

        /* section - last seen */
        cardBody.appendChild(section3);
        section3.appendChild(seen);
        section3.appendChild(seenInfo);
    }
}

function getLocations() {
    showLocations();

    let tbody = document.createElement("tbody");

    for (let i = 0; i < itemsResult.length; i++) {

        let tr = document.createElement("tr");

        let thId = document.createElement("th");
        thId.scope += "row";
        thId.innerHTML = items.results[i].id;

        let tdName = document.createElement("td");
        tdName.innerHTML = items.results[i].name;

        let tdType = document.createElement("td");
        tdType.innerHTML = items.results[i].type;

        let tdDimension = document.createElement("td");
        tdDimension.innerHTML = items.results[i].dimension;

        let tdUrl = document.createElement("td");
        tdUrl.innerHTML = items.results[i].url;


        /*card left*/
        locationsContainer.appendChild(tbody);
        tbody.appendChild(tr);
        tr.appendChild(thId);
        tr.appendChild(tdName);
        tr.appendChild(tdType);
        tr.appendChild(tdDimension);
        tr.appendChild(tdUrl);
    }
}

function getEspisodes() {
    showEpisodes();

    let tbody = document.createElement("tbody");

    for (let i = 0; i < itemsResult.length; i++) {

        let tr = document.createElement("tr");

        let thId = document.createElement("th");
        thId.scope += "row";
        thId.innerHTML = items.results[i].id;

        let tdName = document.createElement("td");
        tdName.innerHTML = items.results[i].name;

        let tdAirDate = document.createElement("td");
        tdAirDate.innerHTML = items.results[i].air_date;

        let tdEpisode = document.createElement("td");
        tdEpisode.innerHTML = items.results[i].episode;

        let tdUrl = document.createElement("td");
        tdUrl.innerHTML = items.results[i].url;


        /*card left*/
        episodesContainer.appendChild(tbody);
        tbody.appendChild(tr);
        tr.appendChild(thId);
        tr.appendChild(tdName);
        tr.appendChild(tdAirDate);
        tr.appendChild(tdEpisode);
        tr.appendChild(tdUrl);
    }
}

function deleteSections() {
    cardsContainer.innerHTML = "";
    episodesContainer.innerHTML = "";
    locationsContainer.innerHTML = "";
    home.classList.toggle("hide", "show");
}

function showCharacters() {
    deleteSections();
    cards.classList.remove("hide");
    episodes.classList.add("hide");
    locations.classList.add("hide");
}

function showLocations() {
    deleteSections();
    locations.classList.remove("hide");
    episodes.classList.add("hide");
    cards.classList.add("hide");
}

function showEpisodes() {
    deleteSections();
    episodes.classList.remove("hide");
    locations.classList.add("hide");
    cards.classList.add("hide");
}


/* footer */
const fCharacter = document.getElementById("fCharacter");
const fLocation = document.getElementById("fLocation");
const fEpisode = document.getElementById("fEpisode");

function showFooterInfo() {
    loadDoc("https://rickandmortyapi.com/api/character", function() {
        fCharacter.innerHTML += items.info.count;
    });

    loadDoc("https://rickandmortyapi.com/api/location", function() {
        fLocation.innerHTML += items.info.count;
    });

    loadDoc("https://rickandmortyapi.com/api/episode", function() {
        fEpisode.innerHTML += items.info.count;
    });
}

showFooterInfo();